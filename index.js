const express = require('express');
// mongoose is a package  that allows us to create Schemas to model our data structures and to manipulate our database using different access methods
const mongoose = require('mongoose');
const app = express();
const port = 3001;

// [SECTION] MongoDB Connection
	// Syntax:
	/*
		mongoose.connect("<MongoDB Atlas Connection String>", 
			{	
				// allows us to avoid any current and future errors while connecting to mongoDB
				useNewUrlParser: true,
				useUnifiedTopology: true
			}
		); 

		// this is to avoid future errors
	*/

mongoose.connect("mongodb+srv://scottgianan:sharicekaie12@wdc028-course-booking.jxpj05l.mongodb.net/b203_to-do?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

	let db = mongoose.connection;

	db.on("error", console.error.bind(console,"connection error"));

	db.once("open", ()=> {
		console.log("We're connected to the cloud database");
	});

	//[SECTION] Mongoose Schemas
		// Schemas determin the structure of the documents to be writtein in the database
		// Schemas act as bluieprints to our data
		// Syntax:
			/*
				const schemaName = new mongoose.Schema({<keyvalue:pair>});
			*/

		// name & status
		// "required" is used to specify that a field must not be empty
		//"default" is used if a field value is not supplied.
		const taskSchema = new mongoose.Schema({
			name : {
				type: String,
				required: [true, "Task name is required"]
			},
			status: {
				type: String,
				default: "pending"
			}
		});

		// [SECTION] Models
			// Uses schema and use it to create/instantiate documents/object that follows our created our schema structure.
			// the variable/object that will be created can be used to run commands for interacting with our database
			// Syntax: const VariableName = mongoose.model("collectionName", schemaName);

			//Using mongoose the package was programmed well enough that i automaticall converts the singlar form of the model name into a plural form

			const Task = mongoose.model("Task", taskSchema);
			/*
				Mini Activity
					1. Create a User schema.
						username - string
						password - string
					2. Create a User model.
			*/
		const userSchema = new mongoose.Schema({
			userName : {
				type: String,
				required: [true, "username is required"]
			},
			password: {
				type: String,
				required: [true, "password is required"]
			}
		});

		const User = mongoose.model("User", userSchema);


//middlewares - they perform a specific function before 
app.use(express.json()); // allows us to read json data
// allows your app to read data from forms
app.use(express.urlencoded({extended:true}));

	app.post("/tasks", (req, res)=>{
		Task.findOne({name: req.body.name}, (err,result) => {
			console.log(result)

				if(result != null && result.name == req.body.name){
                return res.send("Duplicate task found!");               
            	}
            	else {
                    // "newTask" was created/instantiaited from the Mongoose schema and will gain access to ".save" method
                let newTask = new Task({
                    name: req.body.name
                });

                newTask.save((saveErr, savedTask) => {
                    if(saveErr){
                        return console.error(saveErr)
                    }
                    else{
                        return res.status(201).send("New Task created!");
                    }
                })
            }
		});
	});
	app.get("/tasks", (req,res) => {
		Task.find({}, (err, result) => {
			if(err){
				return console.log(err);
			}
			else{
				return res.status(200).send({
					data:result
				})
			}
		});
	})

// ACTIVITY START

/*
Instructions s35 Activity:
1. Using User model.
2. Create a POST route that will access the "/signup" route that will create a user.
5. Process a POST request at the "/signup" route using postman to register a user.
6. Create a git repository named S35.
7. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
8. Add the link in Boodle.

*/

app.post("/signup", (req,res) =>{
	let newUser = new User({
		userName : req.body.userName,
		password : req.body.password 
	});

	newUser.save((saveErr, savedUser) =>{
		if(saveErr){
			return console.error(saveErr)
		}
		else {
			return res.status(200).send("New User Created!")
		}
	})
});


// ACTIVITY END

app.listen(port, () => {
	console.log(`Sever is running at port ${port}`)
});